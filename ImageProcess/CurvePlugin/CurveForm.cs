﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;
using Common;

namespace CurveAdjustPlugin
{
    public partial class CurveForm : Form
    {

        public CurveForm()
        {
            InitializeComponent();
        }

        

        private void CurveForm_Load(object sender, EventArgs e)
        {
            channelComboBox.SelectedIndex = 0;
            cmbThumbSize.Text = ThumnailSize.ToString();
        }

        Bitmap bmp;
        Bitmap bmpThumb;
        Document docPreview = new Document();

        private void deleteButton_Click(object sender, EventArgs e)
        {
            curveControl.DeleteControlPoint();
        }

        int ThumnailSize = 200;

        public void SetBitmap(Bitmap bit)
        {
            bmp = bit;
            bmpThumb = new Bitmap(ThumnailSize, (int)((ThumnailSize / (float)bmp.Width) * bmp.Height));
            Graphics g = Graphics.FromImage(bmpThumb);
            g.DrawImage(bmp, 0, 0, bmpThumb.Width, bmpThumb.Height);
            docPreview.SetBitmap(bmpThumb);
            preview.SetDocument(docPreview);
            preview.ViewMode = ViewMode.OriginalSize;
        }

        public Bitmap GetBitmap()
        {
            return bmp;
        }

        private void curveControl_CurveChanged(object sender, EventArgs e)
        {
            docPreview.SetBitmap(CurveTransform(bmpThumb));
            preview.Refresh();
        }

        private Bitmap CurveTransform(Bitmap bitmap)
        {
            int channel = channelComboBox.SelectedIndex;
            Byte[] map = curveControl.GetColorMap();
            return ImageOperation.ImageSingleTransform(bitmap,
                (ref float r, ref float g, ref float b, int x, int y) =>
                {
                    switch (channel)
                    {
                        case 0:  // RGB
                            {
                                CurveTransformColorValue(ref r, map);
                                CurveTransformColorValue(ref g, map);
                                CurveTransformColorValue(ref b, map);
                            }
                            break;
                        case 1: // R
                            {
                                CurveTransformColorValue(ref r, map);
                            }
                            break;
                        case 2: // G
                            {
                                CurveTransformColorValue(ref g, map);
                            }
                            break;
                        case 3: // B
                            {
                                CurveTransformColorValue(ref b, map);
                            }
                            break;
                    }
                });
        }

        private void CurveTransformColorValue(ref float val, byte[] map)
        {
            val = map[(int)val];
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            bmp = CurveTransform(bmp);
            Cursor = Cursors.Default;
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void channelComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            docPreview.SetBitmap(CurveTransform(bmpThumb));
            preview.Refresh();
        }

        private void cmbThumbSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            ThumnailSize = int.Parse(cmbThumbSize.Text);
            SetBitmap(bmp);
            docPreview.SetBitmap(CurveTransform(bmpThumb));
            preview.Refresh();
        }

    }
}

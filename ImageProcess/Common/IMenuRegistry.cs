﻿using System;
using System.Collections.Generic;

using System.Text;
using System.Windows.Forms;
namespace Common
{
    public interface IMenuRegistry
    {
        void RegisterMenuItem(string ParentMenuString, string MenuString, string MenuID, Keys shortCut, IProcessorPlugin plugin);
    }
}

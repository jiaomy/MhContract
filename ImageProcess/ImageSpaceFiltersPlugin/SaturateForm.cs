﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;

using System.Text;
using System.Windows.Forms;

namespace ImageSpaceFiltersPlugin
{
    public partial class SaturateForm : Form
    {
        public SaturateForm()
        {
            InitializeComponent();
        }

        public event EventHandler ValueChanged = null;

        private void saturateTrackBar_Scroll(object sender, EventArgs e)
        {
            if (ValueChanged != null)
                ValueChanged(sender, e);
        }

        public int GetValue()
        {
            return saturateTrackBar.Value;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}

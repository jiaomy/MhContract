﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Globalization;
using System.Threading;
using PeerPublicLib;
namespace Sbk_Manage
{
    public partial class FrmMain : Form
    {
        #region  声明

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, string lParam);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wparam, string lParam);

        [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, uint wMsg, int wParam, int lParam);

        [DllImport("user32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr SendMessage(IntPtr hwnd, int wMsg, bool wparam, int lParam);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "RedrawWindow")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool RedrawWindow([System.Runtime.InteropServices.InAttribute()] System.IntPtr hWnd, [System.Runtime.InteropServices.InAttribute()] System.IntPtr lprcUpdate, [System.Runtime.InteropServices.InAttribute()] System.IntPtr hrgnUpdate, uint flags);
        [DllImport("User32.dll")]
        private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

        private struct LASTINPUTINFO
        {
            public uint cbSize;

            public uint dwTime;
        }
        LASTINPUTINFO lastInPut = new LASTINPUTINFO();
        Single Lock_Time = 30f;
        private void timer1_Tick(object sender, EventArgs e)
        {
            //配置时间内无操作锁定
            lastInPut.cbSize = (uint)System.Runtime.InteropServices.Marshal.SizeOf(lastInPut);
            GetLastInputInfo(ref lastInPut);

            long noOperationTime = System.Environment.TickCount - lastInPut.dwTime;

            //30分钟不操作自动锁屏
            if (noOperationTime > (Lock_Time * 1000 * 60))
            {
                //锁屏
                Form ins = Application.OpenForms["FrmLock"];
                if (ins == null)
                {
                    ins = new FrmLock();
                    ins.ShowDialog();
                }
            }
        }
        #endregion

        #region 创建mdi子窗体

        private string BasePath = AppDomain.CurrentDomain.BaseDirectory;
        private bool isExist(string ChildTypeName)
        {
            if (Directory.GetCurrentDirectory() != BasePath)
                Directory.SetCurrentDirectory(BasePath);

            bool b_result = false;
            foreach (Form frm in MdiChildren)
            {
                if (frm.GetType().Name == ChildTypeName)
                {
                    if (frm.Tag.Equals("show"))
                    {
                        frm.WindowState = FormWindowState.Maximized;
                        frm.Activate();
                        return true;
                    }
                }
            }
            foreach (Form frm in MdiChildren)
            {
                frm.Hide();
                frm.Tag = "hide";
            }
            foreach (Form frm in MdiChildren)
            {
                if (frm.GetType().Name == ChildTypeName)
                {
                    frm.WindowState = FormWindowState.Maximized;
                    int ret = SendMessage(this.Handle, 0x000B, 0, 0);
                    frm.BringToFront();
                    frm.Show();
                    frm.Tag = "show";
                    frm.Activate();
                    ret = SendMessage(this.Handle, 0x000B, 1, 0);
                    RedrawWindow(this.Handle, IntPtr.Zero, IntPtr.Zero, 0x0491);
                    b_result = true;
                    break;
                }
            }
            return b_result;
        }
        #endregion
        public static string[] args;
        public FrmMain()
        {
            FrmLogin.args = args;
            FrmLogin FrmLoginSL = new FrmLogin();
            FrmLoginSL.BringToFront();
            DialogResult rt = FrmLoginSL.ShowDialog();
            if (rt == DialogResult.Cancel)
            {
                FrmLoginSL.Close();
                FrmLoginSL.Dispose();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }
            else
            {
                FrmLoginSL.Close();
                FrmLoginSL.Dispose();
            }
            InitializeComponent();
            //
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.ResizeRedraw | ControlStyles.AllPaintingInWmPaint, true);
            //给MDI 父窗体添加背景和解决闪烁的问题
            BackgroundNoSplash();
            //修改日期格式
            Thread.CurrentThread.CurrentCulture = new CultureInfo("zh-CN");
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
            //窗体最大化
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            this.CenterToScreen();
            Program.FrmMainIns = this;
        }
        public static MdiClient mdiClient = new MdiClient();

        private void BackgroundNoSplash()
        {
            foreach (Control var in this.Controls)
            {
                if (var is MdiClient)
                {
                    mdiClient = var as MdiClient;
                    break;
                }
            }

            if (mdiClient != null)
            {
                mdiClient.Paint += new PaintEventHandler(OnMdiClientPaint);
                System.Reflection.MethodInfo mi = (mdiClient as Control).GetType().GetMethod("SetStyle", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                mi.Invoke(mdiClient, new object[] { ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer
                 | ControlStyles.ResizeRedraw, true });

            }
        }

        private void OnMdiClientPaint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.DrawImage(Properties.Resources.timg, new Rectangle(0, 0, mdiClient.Width, mdiClient.Height));
            string msg = "山东麦禾信息技术有限公司版权所有©2010-" + DateTime.Now.ToString("yyyy");
            SizeF size = e.Graphics.MeasureString(msg, this.Font);
            g.DrawString(msg, this.Font, new SolidBrush(Color.GreenYellow), mdiClient.Width - size.Width, mdiClient.Height - size.Height);
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            this.menuStrip1.ShowItemToolTips = true;
            BtnOldFont = this.toolStripMenuItem4.Font;
            timer1.Enabled = true;
        }

        private void FrmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.FrmMainIns = null;
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("确定要退出本系统麽？", "麦禾合同管理系统", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Visible = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
    
        private void FrmMain_Shown(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                this.Show();
            }
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            this.BringToFront();
            this.Activate();
        }
        //隐藏子窗体的最大最小
        private void menuStrip1_ItemAdded(object sender, ToolStripItemEventArgs e)
        {
            if (e.Item.Text.Length == 0 || e.Item.Text == "还原(&R)" || e.Item.Text == "最小化(&N)")
            {
                e.Item.Visible = false;
            }
        }
        //退出系统
        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //合同管理
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem BtnIns = (ToolStripMenuItem)sender;
            BtnForColorSet(BtnIns);
            PeerPublicLib.FormProcess.CreateFormInstance("FrmContract", this, Program.FrmMainIns);
        }
        //修改密码
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Frm_Pwd Frm_PwdIns = new Frm_Pwd();
            Frm_PwdIns.ShowDialog();
        }
        //最小化
        private void toolStripMenuItem8_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        //综合查询
        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem BtnIns = (ToolStripMenuItem)sender;
            BtnForColorSet(BtnIns);
            PeerPublicLib.FormProcess.CreateFormInstance("Frm_Zhcx", this, Program.FrmMainIns);
        }
        //设备管理 
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem BtnIns = (ToolStripMenuItem)sender;
            BtnForColorSet(BtnIns);
            PeerPublicLib.FormProcess.CreateFormInstance("Frm_DeviceManager", this, Program.FrmMainIns);
        }
        //统计数据 
        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem BtnIns = (ToolStripMenuItem)sender;
            BtnForColorSet(BtnIns);
            PeerPublicLib.FormProcess.CreateFormInstance("Ftm_TjData", this, Program.FrmMainIns);
        }
        //使用帮助 
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem BtnIns = (ToolStripMenuItem)sender;
            BtnForColorSet(BtnIns);
            PeerPublicLib.FormProcess.CreateFormInstance("Frm_Help", this, Program.FrmMainIns);
        }
        private Font BtnOldFont;
        public void BtnForColorSet(ToolStripMenuItem BtnIns)
        {
            //绑定事件处理
            for (int i = 0; i < this.menuStrip1.Items.Count; i++)
            {
                ToolStripMenuItem BtnItem = ((ToolStripMenuItem)this.menuStrip1.Items[i]);
                BtnItem.Font = BtnOldFont;
            }
            if (BtnIns != null)
            {
                BtnIns.Font = new Font(BtnOldFont,FontStyle.Bold);
            }
        }
        private void toolStripMenuItem10_Click_1(object sender, EventArgs e)
        {
            Frm_Super ins = new Frm_Super();
            ins.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class FrmLock : Form
    {
        public FrmLock()
        {
            InitializeComponent();
        }

        private void FrmLock_Load(object sender, EventArgs e)
        {
            UsernameTextBox.Text = Program.User_Name;
            this.ActiveControl = PasswordTextBox;
            PasswordTextBox.Select();
            PasswordTextBox.Focus();
        }

        private void PasswordTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == Convert.ToChar(System.Windows.Forms.Keys.Enter))
            {
                if (PasswordTextBox.Text.ToString().Equals(Program.User_Pwd))
                {
                    this.Close();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.maihe.info/");
        }
        //退出
        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
            Program.FrmMainIns.Close();
            //强制资源回收
            GC.Collect();
            //退出当前进程
            System.Diagnostics.Process.GetCurrentProcess().Kill();
        }
    }
}

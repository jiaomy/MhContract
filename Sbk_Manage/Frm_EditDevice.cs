﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Sbk_Manage
{
    public partial class Frm_EditDevice : Form
    {
        public Frm_EditDevice()
        {
            InitializeComponent();
        }
        public int CursbID = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            //验收日期 入账日期
            string ysrq = "";
            string rzrq = "";
            int Itf = 0;
            int Ijk = 0;
            string xcjcrq = "";
            string bfrq = "";
            int ibf=0;
            if (txtHtbh.Text.Trim().Equals(""))
            {
                MessageBox.Show("合同编号不能为空！");
                txtHtbh.Focus();
                return;
            }
            if(txtSbmc.Text.Trim().Equals(""))
            {
                MessageBox.Show("设备名称不能为空！");
                txtSbmc.Focus();
                return;
            }
            if (chkJk.Checked)
            {
                if (txtWmgsmc.Text.Trim().Equals("") || this.txtBgd.Text.Trim().Equals("") || this.txtJyjyzm.Text.Trim().Equals(""))
                {
                    MessageBox.Show("进口设备【外贸公司、报关单、检验检疫】三个都不能为空值！");
                    return;
                }
            }
            if (chkBfwh.Checked)
            {
                if (txtBfwh.Text.Trim().Equals(""))
                {
                    MessageBox.Show("报废文号不能为空值！");
                    return;
                }
                ibf=1;
                bfrq = this.dtpBfrq.Value.ToString("yyyy-MM-dd");
            }
            //
            if(cmbSblb.Text.Equals("计量") || cmbSblb.Text.Equals("特种"))
            {
                if (dtpjyrq.Value.ToString("yyyy-MM-dd").Equals(DateTime.Now.ToString("yyyy-MM-dd")))
                {
                    MessageBox.Show("下次检测日期不能为当天！");
                    return;
                }
                xcjcrq = dtpjyrq.Value.ToString("yyyy-MM-dd");
            }
            else if (cmbSblb.Text.Equals("射线装置"))
            {
                if (textSxzz.Text.Trim().Equals(""))
                {
                    MessageBox.Show("射线装置职业病预控评不能为空值！");
                    return;
                }
            }
            if (chkJk.Checked)
            {
                Ijk = 1;
            }
            if (chkTf.Checked)
            {
                Itf = 1;
            }
            if (chkYsrq.Checked)
            {
                ysrq = this.dtpYsrq.Value.ToString("yyyy-MM-dd");
            }
            if (chkRzrq.Checked)
            {
                rzrq = this.dtpRzrq.Value.ToString("yyyy-MM-dd");
            }

            string sqlstr = string.Format("update sb_info set mc='{0}',sbpy='{1}',xhgg='{2}',sbsn='{3}',barcode='{4}',bp='{5}',zczh='{6}',syks='{7}',ghsmc='{8}',ghslxfs='{9}',cjmc='{10}',cjlxfs='{11}',ysrq='{12}',rzrq='{13}',zbq='{14}',sbsl='{15}',danjia='{16}',memo_note='{17}',sblb='{18}',tf_flag='{19}',jk_flag='{20}',wmgsmc='{21}',bgd='{22}',jyjyzm='{23}',xcjyrq='{24}',zybkzpjb='{25}',bf_flag='{26}',bfrq='{27}',bfwjph='{28}',htbh='{29}' where id=" + CursbID, this.txtSbmc.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(this.txtSbmc.Text.Trim().Replace("'", "")), this.txtXhgg.Text.Trim().Replace("'", ""), this.txtCcbh.Text.Trim().Replace("'", ""), this.txtSbtm.Text.Trim().Replace("'", ""), this.txtSbpp.Text.Trim().Replace("'", ""), this.txtZczh.Text.Trim().Replace("'", ""), this.txtSyks.Text.Trim().Replace("'", ""), this.txtGhs.Text.Trim().Replace("'", ""), this.txtGhslxfs.Text.Trim().Replace("'", ""), this.txtCj.Text.Trim().Replace("'", ""), this.txtCjlxfs.Text.Trim().Replace("'", ""), ysrq, rzrq, this.txtZbq.Text.Trim().Replace("'", ""), txtSbsl.Text.Trim(), txtSbdj.Text.Trim(), this.txtSbbzsm.Text.Trim().Replace("'", ""), cmbSblb.Text, Itf, Ijk, this.txtWmgsmc.Text.Trim(), this.txtBgd.Text.Trim(), this.txtJyjyzm.Text.Trim().Replace("'", ""), xcjcrq, this.textSxzz.Text.Trim().Replace("'", ""), ibf, bfrq, this.txtBfwh.Text.Trim().Replace("'", ""), this.txtHtbh.Text.Trim().Replace("'", ""));
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                MessageBox.Show("设备信息更新成功!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }

        }
        private void txtSbdj_LostFocus(object sender, EventArgs e)
        {
            if (!txtSbdj.Text.Trim().Equals(""))
            {
                txtSbdj.Text = ToDBC(txtSbdj.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(txtSbdj.Text.Trim()) == false)
                {
                    txtSbdj.Text = "";
                    txtSbdj.Focus();
                }
            }
            else
            {
                txtSbdj.Text = "0";
            }
        }
        private void txtSbsl_LostFocus(object sender, EventArgs e)
        {
            if (!txtSbsl.Text.Trim().Equals(""))
            {
                txtSbsl.Text = ToDBC(txtSbsl.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(txtSbsl.Text.Trim()) == false)
                {
                    txtSbsl.Text = "";
                    txtSbsl.Focus();
                }
            }
            else
            {
                txtSbsl.Text = "";
            }
        }
        ///转半角的函数(DBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248// 
        public static string ToDBC(string input)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 12288)
                {
                    array[i] = (char)32;
                    continue;
                }
                if (array[i] > 65280 && array[i] < 65375)
                {
                    array[i] = (char)(array[i] - 65248);
                }
            }
            return new string(array);
        }
     
        private void Frm_AddDevice_Load(object sender, EventArgs e)
        {
            cmbSblb.SelectedIndex = 0;
            this.txtSbdj.LostFocus += new EventHandler(txtSbdj_LostFocus);
            this.txtSbsl.LostFocus += new EventHandler(txtSbsl_LostFocus);
            //赋值
            string sqlstr = "select id,strftime('%Y-%m-%d',ysrq) as ysrq,strftime('%Y-%m-%d',rzrq) as rzrq,strftime('%Y-%m-%d',bfrq) as bfrq,strftime('%Y-%m-%d',xcjyrq) as xcjyrq,mc,xhgg,sbsn,barcode,bp,zczh,syks,ghsmc,ghslxfs,cjmc,cjlxfs,zbq,sbsl,danjia,memo_note,sblb,tf_flag,jk_flag,wmgsmc,bgd,jyjyzm,zybkzpjb,bf_flag,bfwjph,htbh from sb_info where id=" + CursbID;
            DataTable dt = Program.SqliteDB.GetDataTable(sqlstr);
            if (dt.Rows.Count > 0)
            {
                txtHtbh.Text = dt.Rows[0]["htbh"].ToString();
                this.txtSbmc.Text = dt.Rows[0]["mc"].ToString();
                this.txtXhgg.Text = dt.Rows[0]["xhgg"].ToString();
                this.txtCcbh.Text = dt.Rows[0]["sbsn"].ToString();
                this.txtSbtm.Text = dt.Rows[0]["barcode"].ToString();
                this.txtSbpp.Text = dt.Rows[0]["bp"].ToString();
                this.txtZczh.Text = dt.Rows[0]["zczh"].ToString();
                this.txtSyks.Text = dt.Rows[0]["syks"].ToString();
                this.txtGhs.Text = dt.Rows[0]["ghsmc"].ToString();
                this.txtGhslxfs.Text = dt.Rows[0]["ghslxfs"].ToString();
                this.txtCj.Text = dt.Rows[0]["cjmc"].ToString();
                this.txtCjlxfs.Text = dt.Rows[0]["cjlxfs"].ToString();
                this.txtZbq.Text = dt.Rows[0]["zbq"].ToString();
                this.txtSbsl.Text = dt.Rows[0]["sbsl"].ToString();
                this.txtSbdj.Text = dt.Rows[0]["danjia"].ToString();
                this.txtSbbzsm.Text = dt.Rows[0]["memo_note"].ToString();
                this.txtWmgsmc.Text = dt.Rows[0]["wmgsmc"].ToString();
                cmbSblb.Text = dt.Rows[0]["sblb"].ToString();
                this.txtBgd.Text = dt.Rows[0]["bgd"].ToString();
                this.txtJyjyzm.Text = dt.Rows[0]["jyjyzm"].ToString();
                this.textSxzz.Text = dt.Rows[0]["zybkzpjb"].ToString();
                //
                if (dt.Rows[0]["jk_flag"].ToString().Equals("1"))
                {
                    this.chkJk.Checked = true;
                }
                if (dt.Rows[0]["tf_flag"].ToString().Equals("1"))
                {
                    this.chkTf.Checked = true;
                }
                if (!dt.Rows[0]["xcjyrq"].ToString().Equals(""))
                {
                    this.dtpjyrq.Value = Convert.ToDateTime(dt.Rows[0]["xcjyrq"].ToString());
                }
                if (dt.Rows[0]["rzrq"].ToString().Equals(""))
                {
                    this.chkRzrq.Checked = false;
                }
                else
                {
                    this.chkRzrq.Checked = true;
                    this.dtpRzrq.Value = Convert.ToDateTime(dt.Rows[0]["rzrq"].ToString());
                }
                if (dt.Rows[0]["ysrq"].ToString().Equals(""))
                {
                    this.chkYsrq.Checked = false;
                }
                else
                {
                    this.chkYsrq.Checked = true;
                    this.dtpYsrq.Value = Convert.ToDateTime(dt.Rows[0]["ysrq"].ToString());
                }
                if (dt.Rows[0]["bfrq"].ToString().Equals(""))
                {
                    this.chkBfwh.Checked = false;
                }
                else
                {
                    this.chkBfwh.Checked = true;
                    this.dtpBfrq.Value = Convert.ToDateTime(dt.Rows[0]["bfrq"].ToString());
                    this.txtBfwh.Text = dt.Rows[0]["bfwjph"].ToString();
                }
            }
            else
            {
                this.Enabled = false;
            }
        }

        
    }
}

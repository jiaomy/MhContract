﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace Sbk_Manage
{
    public partial class Frm_AddHt : Form
    {
        public Frm_AddHt()
        {
            InitializeComponent();
            this.KeyPreview = true;
            //修改日期格式
            Thread.CurrentThread.CurrentCulture = new CultureInfo("zh-CN");
            Thread.CurrentThread.CurrentCulture = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd HH:mm:ss";
        }

        private void Frm_AddHt_Load(object sender, EventArgs e)
        {
            txtLrr.Text = Program.User_Name;
            TxtHte.LostFocus += new EventHandler(TxtHte_LostFocus);
        }
        ///转半角的函数(DBC case)
        ///全角空格为12288，半角空格为32
        ///其他字符半角(33-126)与全角(65281-65374)的对应关系是：均相差65248// 
        public static string ToDBC(string input)
        {
            char[] array = input.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == 12288)
                {
                    array[i] = (char)32;
                    continue;
                }
                if (array[i] > 65280 && array[i] < 65375)
                {
                    array[i] = (char)(array[i] - 65248);
                }
            }
            return new string(array);
        }
        private void TxtHte_LostFocus(object sender, EventArgs e)
        {
            if (!TxtHte.Text.Trim().Equals(""))
            {
                TxtHte.Text = ToDBC(TxtHte.Text);
                if (Microsoft.VisualBasic.Information.IsNumeric(TxtHte.Text.Trim()) == false)
                {
                    TxtHte.Text = "";
                    TxtHte.Focus();
                }
            }
            else
            {
                TxtHte.Text = "0";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Focus();
            if (txtHtbh.Text.Trim().Equals("") )
            {
                MessageBox.Show("合同编号不允许为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHtbh.Focus();
                return;
            }
            if (txtHtmc.Text.Trim().Equals(""))
            {
                MessageBox.Show("合同名称不允许为空!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHtmc.Focus();
                return;
            }
            //查询合同编号是否已经存在
            Boolean RetBool=  Program.SqliteDB.GetSqlReader("select * from ht_info where del_flag=0 and htbh='"+txtHtbh.Text.Trim().Replace("'","")+"'");
            if (RetBool)
            {
                MessageBox.Show("合同编号已经存在，请核实!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHtbh.Focus();
                return;
            }
            string sqlstr = "";
            if (chKYys.Checked == true)
            {
                sqlstr = string.Format("insert into ht_info values(NULL,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}')", txtHtbh.Text.Trim().Replace("'", ""), txtXmbh.Text.Trim().Replace("'", ""), txtZbdlgs.Text.Trim().Replace("'", ""), txtHtmc.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(txtHtmc.Text.Trim().Replace("'", "")), txtGhs.Text.Trim().Replace("'", ""), TxtHte.Text.Trim().Replace("'", ""), dtpHtQdrq.Value.Date.ToString("s"), dtpHtYsrq.Value.Date.ToString("s"), txtGhsYhk.Text.Trim().Replace("'", ""), DateTime.Now.ToString("s"), txtLrr.Text.Trim().Replace("'", ""), 0, 0, 0);
            }
            else
            {
                sqlstr = string.Format("insert into ht_info values(NULL,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',NULL,'{8}','{9}','{10}','{11}','{12}','{13}')", txtHtbh.Text.Trim().Replace("'", ""), txtXmbh.Text.Trim().Replace("'", ""), txtZbdlgs.Text.Trim().Replace("'", ""), txtHtmc.Text.Trim().Replace("'", ""), PinYinConverter.GetFirst(txtHtmc.Text.Trim().Replace("'", "")), txtGhs.Text.Trim().Replace("'", ""), TxtHte.Text.Trim().Replace("'", ""), dtpHtQdrq.Value.Date.ToString("s"), txtGhsYhk.Text.Trim().Replace("'", ""), DateTime.Now.ToString("s"), txtLrr.Text.Trim().Replace("'", ""), 0, 0, 0);
            }
            if (Program.SqliteDB.ExecuteNonQuery(sqlstr) == 1)
            {
                MessageBox.Show("合同添加成功!", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
        }
    }
}

﻿namespace Sbk_Manage
{
    partial class Frm_Zhcx
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Zhcx));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TxtSbsyks = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpSbysrq = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxtSbCj = new System.Windows.Forms.TextBox();
            this.Txtghs = new System.Windows.Forms.TextBox();
            this.BtnQuery = new System.Windows.Forms.Button();
            this.TxtHtBh = new System.Windows.Forms.TextBox();
            this.TxtHtMc = new System.Windows.Forms.TextBox();
            this.TxtSbMc = new System.Windows.Forms.TextBox();
            this.ChkSbysrq = new System.Windows.Forms.CheckBox();
            this.TxtSbBfpzwh = new System.Windows.Forms.TextBox();
            this.BtnPreview = new System.Windows.Forms.Button();
            this.BtnNext = new System.Windows.Forms.Button();
            this.BtnReset = new System.Windows.Forms.Button();
            this.TxtSbBarcode = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TxtHtlrnf = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DgvSbInfo = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xmbh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xhgg = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.syks = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sbsl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.danjia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zbdlgs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.hte = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htlrr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.scan_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jc_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sbsn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zczh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghsmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghskpxx = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ghslxfs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cjmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cjlxfs = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ysrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rzrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zbq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.memo_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sblb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tf_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jk_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bgd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wmgsmc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.jyjyzm = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.xcjyrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.zybkzpjb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bf_flag = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htqdrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htysrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.htlrrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bfrq = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bfwjph = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.TableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSbInfo)).BeginInit();
            this.StatusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.SplitContainer1.IsSplitterFixed = true;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.TableLayoutPanel1);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.DgvSbInfo);
            this.SplitContainer1.Panel2.Controls.Add(this.StatusStrip1);
            this.SplitContainer1.Size = new System.Drawing.Size(1403, 581);
            this.SplitContainer1.SplitterDistance = 152;
            this.SplitContainer1.SplitterWidth = 1;
            this.SplitContainer1.TabIndex = 5;
            this.SplitContainer1.TabStop = false;
            // 
            // TableLayoutPanel1
            // 
            this.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.TableLayoutPanel1.ColumnCount = 6;
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.TableLayoutPanel1.Controls.Add(this.TxtSbsyks, 4, 1);
            this.TableLayoutPanel1.Controls.Add(this.label8, 4, 0);
            this.TableLayoutPanel1.Controls.Add(this.dtpSbysrq, 3, 3);
            this.TableLayoutPanel1.Controls.Add(this.label7, 2, 2);
            this.TableLayoutPanel1.Controls.Add(this.label6, 1, 2);
            this.TableLayoutPanel1.Controls.Add(this.label5, 0, 2);
            this.TableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.TableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.TableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.TableLayoutPanel1.Controls.Add(this.TxtSbCj, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.Txtghs, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.BtnQuery, 5, 0);
            this.TableLayoutPanel1.Controls.Add(this.TxtHtBh, 0, 1);
            this.TableLayoutPanel1.Controls.Add(this.TxtHtMc, 1, 1);
            this.TableLayoutPanel1.Controls.Add(this.TxtSbMc, 2, 1);
            this.TableLayoutPanel1.Controls.Add(this.ChkSbysrq, 3, 2);
            this.TableLayoutPanel1.Controls.Add(this.TxtSbBfpzwh, 0, 3);
            this.TableLayoutPanel1.Controls.Add(this.BtnPreview, 5, 1);
            this.TableLayoutPanel1.Controls.Add(this.BtnNext, 5, 2);
            this.TableLayoutPanel1.Controls.Add(this.BtnReset, 5, 3);
            this.TableLayoutPanel1.Controls.Add(this.TxtSbBarcode, 3, 1);
            this.TableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.TableLayoutPanel1.Controls.Add(this.TxtHtlrnf, 4, 3);
            this.TableLayoutPanel1.Controls.Add(this.label9, 4, 2);
            this.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.TableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.TableLayoutPanel1.Name = "TableLayoutPanel1";
            this.TableLayoutPanel1.RowCount = 4;
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.TableLayoutPanel1.Size = new System.Drawing.Size(1399, 148);
            this.TableLayoutPanel1.TabIndex = 0;
            // 
            // TxtSbsyks
            // 
            this.TxtSbsyks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSbsyks.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtSbsyks.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtSbsyks.Location = new System.Drawing.Point(934, 42);
            this.TxtSbsyks.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSbsyks.MaxLength = 100;
            this.TxtSbsyks.Name = "TxtSbsyks";
            this.TxtSbsyks.Size = new System.Drawing.Size(222, 26);
            this.TxtSbsyks.TabIndex = 5;
            this.TxtSbsyks.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(933, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(224, 34);
            this.label8.TabIndex = 35;
            this.label8.Text = "设备使用科室";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dtpSbysrq
            // 
            this.dtpSbysrq.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dtpSbysrq.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.dtpSbysrq.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSbysrq.Location = new System.Drawing.Point(702, 116);
            this.dtpSbysrq.Margin = new System.Windows.Forms.Padding(4);
            this.dtpSbysrq.Name = "dtpSbysrq";
            this.dtpSbysrq.Size = new System.Drawing.Size(222, 26);
            this.dtpSbysrq.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(469, 74);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(224, 34);
            this.label7.TabIndex = 33;
            this.label7.Text = "报废批准文号";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(237, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(224, 34);
            this.label6.TabIndex = 32;
            this.label6.Text = "厂家";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(5, 74);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(224, 34);
            this.label5.TabIndex = 31;
            this.label5.Text = "供货商";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(701, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(224, 34);
            this.label4.TabIndex = 30;
            this.label4.Text = "设备管理条码";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(469, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(224, 34);
            this.label3.TabIndex = 29;
            this.label3.Text = "设备名称";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(237, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(224, 34);
            this.label2.TabIndex = 28;
            this.label2.Text = "合同名称";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtSbCj
            // 
            this.TxtSbCj.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSbCj.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtSbCj.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtSbCj.Location = new System.Drawing.Point(6, 116);
            this.TxtSbCj.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSbCj.MaxLength = 100;
            this.TxtSbCj.Name = "TxtSbCj";
            this.TxtSbCj.Size = new System.Drawing.Size(222, 26);
            this.TxtSbCj.TabIndex = 7;
            this.TxtSbCj.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Txtghs
            // 
            this.Txtghs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Txtghs.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Txtghs.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Txtghs.Location = new System.Drawing.Point(238, 116);
            this.Txtghs.Margin = new System.Windows.Forms.Padding(4);
            this.Txtghs.MaxLength = 100;
            this.Txtghs.Name = "Txtghs";
            this.Txtghs.Size = new System.Drawing.Size(222, 26);
            this.Txtghs.TabIndex = 6;
            this.Txtghs.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnQuery
            // 
            this.BtnQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnQuery.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnQuery.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnQuery.Image = ((System.Drawing.Image)(resources.GetObject("BtnQuery.Image")));
            this.BtnQuery.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnQuery.Location = new System.Drawing.Point(1166, 6);
            this.BtnQuery.Margin = new System.Windows.Forms.Padding(4);
            this.BtnQuery.Name = "BtnQuery";
            this.BtnQuery.Size = new System.Drawing.Size(227, 26);
            this.BtnQuery.TabIndex = 13;
            this.BtnQuery.Text = "查询";
            this.BtnQuery.UseVisualStyleBackColor = true;
            this.BtnQuery.Click += new System.EventHandler(this.BtnQuery_Click);
            // 
            // TxtHtBh
            // 
            this.TxtHtBh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHtBh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHtBh.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHtBh.Location = new System.Drawing.Point(6, 42);
            this.TxtHtBh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHtBh.MaxLength = 100;
            this.TxtHtBh.Name = "TxtHtBh";
            this.TxtHtBh.Size = new System.Drawing.Size(222, 26);
            this.TxtHtBh.TabIndex = 1;
            this.TxtHtBh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtHtMc
            // 
            this.TxtHtMc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtHtMc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtHtMc.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtHtMc.Location = new System.Drawing.Point(238, 42);
            this.TxtHtMc.Margin = new System.Windows.Forms.Padding(4);
            this.TxtHtMc.MaxLength = 100;
            this.TxtHtMc.Name = "TxtHtMc";
            this.TxtHtMc.Size = new System.Drawing.Size(222, 26);
            this.TxtHtMc.TabIndex = 2;
            this.TxtHtMc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TxtSbMc
            // 
            this.TxtSbMc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSbMc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtSbMc.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtSbMc.Location = new System.Drawing.Point(470, 42);
            this.TxtSbMc.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSbMc.MaxLength = 100;
            this.TxtSbMc.Name = "TxtSbMc";
            this.TxtSbMc.Size = new System.Drawing.Size(222, 26);
            this.TxtSbMc.TabIndex = 3;
            this.TxtSbMc.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ChkSbysrq
            // 
            this.ChkSbysrq.AutoSize = true;
            this.ChkSbysrq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkSbysrq.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.ChkSbysrq.Location = new System.Drawing.Point(702, 78);
            this.ChkSbysrq.Margin = new System.Windows.Forms.Padding(4);
            this.ChkSbysrq.Name = "ChkSbysrq";
            this.ChkSbysrq.Size = new System.Drawing.Size(222, 26);
            this.ChkSbysrq.TabIndex = 9;
            this.ChkSbysrq.Text = "设备验收日期";
            this.ChkSbysrq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChkSbysrq.UseVisualStyleBackColor = true;
            // 
            // TxtSbBfpzwh
            // 
            this.TxtSbBfpzwh.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSbBfpzwh.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtSbBfpzwh.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtSbBfpzwh.Location = new System.Drawing.Point(470, 116);
            this.TxtSbBfpzwh.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSbBfpzwh.MaxLength = 100;
            this.TxtSbBfpzwh.Name = "TxtSbBfpzwh";
            this.TxtSbBfpzwh.Size = new System.Drawing.Size(222, 26);
            this.TxtSbBfpzwh.TabIndex = 8;
            this.TxtSbBfpzwh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BtnPreview
            // 
            this.BtnPreview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnPreview.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPreview.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnPreview.Image = ((System.Drawing.Image)(resources.GetObject("BtnPreview.Image")));
            this.BtnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPreview.Location = new System.Drawing.Point(1166, 42);
            this.BtnPreview.Margin = new System.Windows.Forms.Padding(4);
            this.BtnPreview.Name = "BtnPreview";
            this.BtnPreview.Size = new System.Drawing.Size(227, 26);
            this.BtnPreview.TabIndex = 14;
            this.BtnPreview.Text = "上一页";
            this.BtnPreview.UseVisualStyleBackColor = true;
            this.BtnPreview.Click += new System.EventHandler(this.BtnPreview_Click);
            // 
            // BtnNext
            // 
            this.BtnNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnNext.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnNext.Image = ((System.Drawing.Image)(resources.GetObject("BtnNext.Image")));
            this.BtnNext.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnNext.Location = new System.Drawing.Point(1166, 78);
            this.BtnNext.Margin = new System.Windows.Forms.Padding(4);
            this.BtnNext.Name = "BtnNext";
            this.BtnNext.Size = new System.Drawing.Size(227, 26);
            this.BtnNext.TabIndex = 15;
            this.BtnNext.Text = "下一页";
            this.BtnNext.UseVisualStyleBackColor = true;
            this.BtnNext.Click += new System.EventHandler(this.BtnNext_Click);
            // 
            // BtnReset
            // 
            this.BtnReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtnReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnReset.Font = new System.Drawing.Font("宋体", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BtnReset.Image = ((System.Drawing.Image)(resources.GetObject("BtnReset.Image")));
            this.BtnReset.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnReset.Location = new System.Drawing.Point(1166, 114);
            this.BtnReset.Margin = new System.Windows.Forms.Padding(4);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(227, 28);
            this.BtnReset.TabIndex = 16;
            this.BtnReset.Text = "重置查询条件";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // TxtSbBarcode
            // 
            this.TxtSbBarcode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtSbBarcode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.TxtSbBarcode.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TxtSbBarcode.Location = new System.Drawing.Point(702, 42);
            this.TxtSbBarcode.Margin = new System.Windows.Forms.Padding(4);
            this.TxtSbBarcode.MaxLength = 100;
            this.TxtSbBarcode.Name = "TxtSbBarcode";
            this.TxtSbBarcode.Size = new System.Drawing.Size(222, 26);
            this.TxtSbBarcode.TabIndex = 4;
            this.TxtSbBarcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 34);
            this.label1.TabIndex = 27;
            this.label1.Text = "合同编号";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TxtHtlrnf
            // 
            this.TxtHtlrnf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtHtlrnf.Location = new System.Drawing.Point(933, 113);
            this.TxtHtlrnf.MaxLength = 4;
            this.TxtHtlrnf.Name = "TxtHtlrnf";
            this.TxtHtlrnf.Size = new System.Drawing.Size(224, 26);
            this.TxtHtlrnf.TabIndex = 12;
            this.TxtHtlrnf.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("宋体", 9F);
            this.label9.Location = new System.Drawing.Point(933, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(224, 34);
            this.label9.TabIndex = 36;
            this.label9.Text = "合同录入年份";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DgvSbInfo
            // 
            this.DgvSbInfo.AllowUserToAddRows = false;
            this.DgvSbInfo.AllowUserToDeleteRows = false;
            this.DgvSbInfo.AllowUserToResizeColumns = false;
            this.DgvSbInfo.AllowUserToResizeRows = false;
            this.DgvSbInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.DgvSbInfo.BackgroundColor = System.Drawing.Color.White;
            this.DgvSbInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DgvSbInfo.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.DgvSbInfo.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.DgvSbInfo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.htid,
            this.htbh,
            this.xmbh,
            this.htmc,
            this.mc,
            this.xhgg,
            this.bp,
            this.syks,
            this.sbsl,
            this.danjia,
            this.barcode,
            this.zbdlgs,
            this.hte,
            this.htlrr,
            this.scan_flag,
            this.jc_flag,
            this.sbsn,
            this.zczh,
            this.ghsmc,
            this.ghskpxx,
            this.ghslxfs,
            this.cjmc,
            this.cjlxfs,
            this.ysrq,
            this.rzrq,
            this.zbq,
            this.memo_note,
            this.sblb,
            this.tf_flag,
            this.jk_flag,
            this.bgd,
            this.wmgsmc,
            this.jyjyzm,
            this.xcjyrq,
            this.zybkzpjb,
            this.bf_flag,
            this.htqdrq,
            this.htysrq,
            this.htlrrq,
            this.bfrq,
            this.bfwjph});
            this.DgvSbInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DgvSbInfo.Location = new System.Drawing.Point(0, 0);
            this.DgvSbInfo.Margin = new System.Windows.Forms.Padding(4);
            this.DgvSbInfo.MultiSelect = false;
            this.DgvSbInfo.Name = "DgvSbInfo";
            this.DgvSbInfo.ReadOnly = true;
            this.DgvSbInfo.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.DgvSbInfo.RowHeadersWidth = 45;
            this.DgvSbInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.DgvSbInfo.RowTemplate.Height = 23;
            this.DgvSbInfo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DgvSbInfo.Size = new System.Drawing.Size(1399, 402);
            this.DgvSbInfo.TabIndex = 0;
            this.DgvSbInfo.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.DgvSbInfo_CellFormatting);
            this.DgvSbInfo.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.DgvSbInfo_CellMouseDoubleClick);
            this.DgvSbInfo.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.DgvSbInfo_RowPostPaint);
            // 
            // id
            // 
            this.id.DataPropertyName = "id";
            this.id.HeaderText = "设备ID";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 81;
            // 
            // htid
            // 
            this.htid.DataPropertyName = "htid";
            this.htid.HeaderText = "合同ID";
            this.htid.Name = "htid";
            this.htid.ReadOnly = true;
            this.htid.Visible = false;
            this.htid.Width = 81;
            // 
            // htbh
            // 
            this.htbh.DataPropertyName = "htbh";
            this.htbh.HeaderText = "合同编号";
            this.htbh.Name = "htbh";
            this.htbh.ReadOnly = true;
            this.htbh.Width = 97;
            // 
            // xmbh
            // 
            this.xmbh.DataPropertyName = "xmbh";
            this.xmbh.HeaderText = "项目编号";
            this.xmbh.Name = "xmbh";
            this.xmbh.ReadOnly = true;
            this.xmbh.Width = 97;
            // 
            // htmc
            // 
            this.htmc.DataPropertyName = "htmc";
            this.htmc.HeaderText = "合同名称";
            this.htmc.Name = "htmc";
            this.htmc.ReadOnly = true;
            this.htmc.Width = 97;
            // 
            // mc
            // 
            this.mc.DataPropertyName = "mc";
            this.mc.HeaderText = "设备名称";
            this.mc.Name = "mc";
            this.mc.ReadOnly = true;
            this.mc.Width = 97;
            // 
            // xhgg
            // 
            this.xhgg.DataPropertyName = "xhgg";
            this.xhgg.HeaderText = "型号规格";
            this.xhgg.Name = "xhgg";
            this.xhgg.ReadOnly = true;
            this.xhgg.Width = 97;
            // 
            // bp
            // 
            this.bp.DataPropertyName = "bp";
            this.bp.HeaderText = "设备品牌";
            this.bp.Name = "bp";
            this.bp.ReadOnly = true;
            this.bp.Width = 97;
            // 
            // syks
            // 
            this.syks.DataPropertyName = "syks";
            this.syks.HeaderText = "使用科室";
            this.syks.Name = "syks";
            this.syks.ReadOnly = true;
            this.syks.Width = 97;
            // 
            // sbsl
            // 
            this.sbsl.DataPropertyName = "sbsl";
            this.sbsl.HeaderText = "设备数量";
            this.sbsl.Name = "sbsl";
            this.sbsl.ReadOnly = true;
            this.sbsl.Width = 97;
            // 
            // danjia
            // 
            this.danjia.DataPropertyName = "danjia";
            this.danjia.HeaderText = "设备单价(万¥)";
            this.danjia.Name = "danjia";
            this.danjia.ReadOnly = true;
            this.danjia.Width = 137;
            // 
            // barcode
            // 
            this.barcode.DataPropertyName = "barcode";
            this.barcode.HeaderText = "设备管理条码";
            this.barcode.Name = "barcode";
            this.barcode.ReadOnly = true;
            this.barcode.Width = 129;
            // 
            // zbdlgs
            // 
            this.zbdlgs.DataPropertyName = "zbdlgs";
            this.zbdlgs.HeaderText = "招标代理公司";
            this.zbdlgs.Name = "zbdlgs";
            this.zbdlgs.ReadOnly = true;
            this.zbdlgs.Width = 129;
            // 
            // hte
            // 
            this.hte.DataPropertyName = "hte";
            this.hte.HeaderText = "合同总额(万¥)";
            this.hte.Name = "hte";
            this.hte.ReadOnly = true;
            this.hte.Width = 137;
            // 
            // htlrr
            // 
            this.htlrr.DataPropertyName = "htlrr";
            this.htlrr.HeaderText = "合同录入人";
            this.htlrr.Name = "htlrr";
            this.htlrr.ReadOnly = true;
            this.htlrr.Width = 113;
            // 
            // scan_flag
            // 
            this.scan_flag.DataPropertyName = "scan_flag";
            this.scan_flag.HeaderText = "扫描标记";
            this.scan_flag.Name = "scan_flag";
            this.scan_flag.ReadOnly = true;
            this.scan_flag.Width = 97;
            // 
            // jc_flag
            // 
            this.jc_flag.DataPropertyName = "jc_flag";
            this.jc_flag.HeaderText = "借出标记";
            this.jc_flag.Name = "jc_flag";
            this.jc_flag.ReadOnly = true;
            this.jc_flag.Width = 97;
            // 
            // sbsn
            // 
            this.sbsn.DataPropertyName = "sbsn";
            this.sbsn.HeaderText = "出厂编号";
            this.sbsn.Name = "sbsn";
            this.sbsn.ReadOnly = true;
            this.sbsn.Width = 97;
            // 
            // zczh
            // 
            this.zczh.DataPropertyName = "zczh";
            this.zczh.HeaderText = "注册证";
            this.zczh.Name = "zczh";
            this.zczh.ReadOnly = true;
            this.zczh.Width = 81;
            // 
            // ghsmc
            // 
            this.ghsmc.DataPropertyName = "ghsmc";
            this.ghsmc.HeaderText = "供货商";
            this.ghsmc.Name = "ghsmc";
            this.ghsmc.ReadOnly = true;
            this.ghsmc.Width = 81;
            // 
            // ghskpxx
            // 
            this.ghskpxx.DataPropertyName = "ghskpxx";
            this.ghskpxx.HeaderText = "供货商银行账户";
            this.ghskpxx.Name = "ghskpxx";
            this.ghskpxx.ReadOnly = true;
            this.ghskpxx.Width = 145;
            // 
            // ghslxfs
            // 
            this.ghslxfs.DataPropertyName = "ghslxfs";
            this.ghslxfs.HeaderText = "供货商联系方式";
            this.ghslxfs.Name = "ghslxfs";
            this.ghslxfs.ReadOnly = true;
            this.ghslxfs.Width = 145;
            // 
            // cjmc
            // 
            this.cjmc.DataPropertyName = "cjmc";
            this.cjmc.HeaderText = "厂家";
            this.cjmc.MaxInputLength = 10;
            this.cjmc.Name = "cjmc";
            this.cjmc.ReadOnly = true;
            this.cjmc.Width = 65;
            // 
            // cjlxfs
            // 
            this.cjlxfs.DataPropertyName = "cjlxfs";
            this.cjlxfs.HeaderText = "厂家联系方式";
            this.cjlxfs.MaxInputLength = 10;
            this.cjlxfs.Name = "cjlxfs";
            this.cjlxfs.ReadOnly = true;
            this.cjlxfs.Width = 129;
            // 
            // ysrq
            // 
            this.ysrq.DataPropertyName = "ysrq";
            this.ysrq.HeaderText = "设备验收日期";
            this.ysrq.MaxInputLength = 19;
            this.ysrq.Name = "ysrq";
            this.ysrq.ReadOnly = true;
            this.ysrq.Width = 129;
            // 
            // rzrq
            // 
            this.rzrq.DataPropertyName = "rzrq";
            this.rzrq.HeaderText = "设备入账日期";
            this.rzrq.Name = "rzrq";
            this.rzrq.ReadOnly = true;
            this.rzrq.Width = 129;
            // 
            // zbq
            // 
            this.zbq.DataPropertyName = "zbq";
            this.zbq.HeaderText = "质保期";
            this.zbq.Name = "zbq";
            this.zbq.ReadOnly = true;
            this.zbq.Width = 81;
            // 
            // memo_note
            // 
            this.memo_note.DataPropertyName = "memo_note";
            this.memo_note.HeaderText = "备注说明";
            this.memo_note.Name = "memo_note";
            this.memo_note.ReadOnly = true;
            this.memo_note.Width = 97;
            // 
            // sblb
            // 
            this.sblb.DataPropertyName = "sblb";
            this.sblb.HeaderText = "设备类别";
            this.sblb.Name = "sblb";
            this.sblb.ReadOnly = true;
            this.sblb.Width = 97;
            // 
            // tf_flag
            // 
            this.tf_flag.DataPropertyName = "tf_flag";
            this.tf_flag.HeaderText = "投放标记";
            this.tf_flag.Name = "tf_flag";
            this.tf_flag.ReadOnly = true;
            this.tf_flag.Width = 97;
            // 
            // jk_flag
            // 
            this.jk_flag.DataPropertyName = "jk_flag";
            this.jk_flag.HeaderText = "进口标记";
            this.jk_flag.Name = "jk_flag";
            this.jk_flag.ReadOnly = true;
            this.jk_flag.Width = 97;
            // 
            // bgd
            // 
            this.bgd.DataPropertyName = "bgd";
            this.bgd.HeaderText = "报关单";
            this.bgd.Name = "bgd";
            this.bgd.ReadOnly = true;
            this.bgd.Width = 81;
            // 
            // wmgsmc
            // 
            this.wmgsmc.DataPropertyName = "wmgsmc";
            this.wmgsmc.HeaderText = "外贸公司名称";
            this.wmgsmc.Name = "wmgsmc";
            this.wmgsmc.ReadOnly = true;
            this.wmgsmc.Width = 129;
            // 
            // jyjyzm
            // 
            this.jyjyzm.DataPropertyName = "jyjyzm";
            this.jyjyzm.HeaderText = "检验检疫证明";
            this.jyjyzm.Name = "jyjyzm";
            this.jyjyzm.ReadOnly = true;
            this.jyjyzm.Width = 129;
            // 
            // xcjyrq
            // 
            this.xcjyrq.DataPropertyName = "xcjyrq";
            this.xcjyrq.HeaderText = "特种设备下次检验日期";
            this.xcjyrq.Name = "xcjyrq";
            this.xcjyrq.ReadOnly = true;
            this.xcjyrq.Width = 193;
            // 
            // zybkzpjb
            // 
            this.zybkzpjb.DataPropertyName = "zybkzpjb";
            this.zybkzpjb.HeaderText = "射线装置预控评";
            this.zybkzpjb.Name = "zybkzpjb";
            this.zybkzpjb.ReadOnly = true;
            this.zybkzpjb.Width = 145;
            // 
            // bf_flag
            // 
            this.bf_flag.DataPropertyName = "bf_flag";
            this.bf_flag.HeaderText = "报废标记";
            this.bf_flag.Name = "bf_flag";
            this.bf_flag.ReadOnly = true;
            this.bf_flag.Width = 97;
            // 
            // htqdrq
            // 
            this.htqdrq.DataPropertyName = "htqdrq";
            this.htqdrq.HeaderText = "合同签订日期";
            this.htqdrq.Name = "htqdrq";
            this.htqdrq.ReadOnly = true;
            this.htqdrq.Width = 129;
            // 
            // htysrq
            // 
            this.htysrq.DataPropertyName = "htysrq";
            this.htysrq.HeaderText = "合同验收日期";
            this.htysrq.Name = "htysrq";
            this.htysrq.ReadOnly = true;
            this.htysrq.Width = 129;
            // 
            // htlrrq
            // 
            this.htlrrq.DataPropertyName = "htlrrq";
            this.htlrrq.HeaderText = "合同录入日期";
            this.htlrrq.Name = "htlrrq";
            this.htlrrq.ReadOnly = true;
            this.htlrrq.Width = 129;
            // 
            // bfrq
            // 
            this.bfrq.DataPropertyName = "bfrq";
            this.bfrq.HeaderText = "报废日期";
            this.bfrq.Name = "bfrq";
            this.bfrq.ReadOnly = true;
            this.bfrq.Width = 97;
            // 
            // bfwjph
            // 
            this.bfwjph.DataPropertyName = "bfwjph";
            this.bfwjph.HeaderText = "报废设备批准文号";
            this.bfwjph.Name = "bfwjph";
            this.bfwjph.ReadOnly = true;
            this.bfwjph.Width = 161;
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel1});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 402);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.StatusStrip1.Size = new System.Drawing.Size(1399, 22);
            this.StatusStrip1.SizingGrip = false;
            this.StatusStrip1.Stretch = false;
            this.StatusStrip1.TabIndex = 3;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel1
            // 
            this.ToolStripStatusLabel1.Image = ((System.Drawing.Image)(resources.GetObject("ToolStripStatusLabel1.Image")));
            this.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1";
            this.ToolStripStatusLabel1.Size = new System.Drawing.Size(16, 17);
            // 
            // Frm_Zhcx
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1403, 581);
            this.Controls.Add(this.SplitContainer1);
            this.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_Zhcx";
            this.Text = "综合信息查询";
            this.Load += new System.EventHandler(this.Frm_Zhcx_Load);
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            this.SplitContainer1.Panel2.PerformLayout();
            this.SplitContainer1.ResumeLayout(false);
            this.TableLayoutPanel1.ResumeLayout(false);
            this.TableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DgvSbInfo)).EndInit();
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.SplitContainer SplitContainer1;
        internal System.Windows.Forms.TableLayoutPanel TableLayoutPanel1;
        internal System.Windows.Forms.TextBox TxtSbsyks;
        private System.Windows.Forms.Label label8;
        internal System.Windows.Forms.DateTimePicker dtpSbysrq;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.TextBox TxtSbCj;
        internal System.Windows.Forms.TextBox Txtghs;
        internal System.Windows.Forms.Button BtnQuery;
        internal System.Windows.Forms.TextBox TxtHtBh;
        internal System.Windows.Forms.TextBox TxtHtMc;
        internal System.Windows.Forms.TextBox TxtSbMc;
        internal System.Windows.Forms.CheckBox ChkSbysrq;
        internal System.Windows.Forms.TextBox TxtSbBfpzwh;
        internal System.Windows.Forms.Button BtnPreview;
        internal System.Windows.Forms.Button BtnNext;
        internal System.Windows.Forms.Button BtnReset;
        internal System.Windows.Forms.TextBox TxtSbBarcode;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.DataGridView DgvSbInfo;
        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel1;
        private System.Windows.Forms.TextBox TxtHtlrnf;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn htid;
        private System.Windows.Forms.DataGridViewTextBoxColumn htbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn xmbh;
        private System.Windows.Forms.DataGridViewTextBoxColumn htmc;
        private System.Windows.Forms.DataGridViewTextBoxColumn mc;
        private System.Windows.Forms.DataGridViewTextBoxColumn xhgg;
        private System.Windows.Forms.DataGridViewTextBoxColumn bp;
        private System.Windows.Forms.DataGridViewTextBoxColumn syks;
        private System.Windows.Forms.DataGridViewTextBoxColumn sbsl;
        private System.Windows.Forms.DataGridViewTextBoxColumn danjia;
        private System.Windows.Forms.DataGridViewTextBoxColumn barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn zbdlgs;
        private System.Windows.Forms.DataGridViewTextBoxColumn hte;
        private System.Windows.Forms.DataGridViewTextBoxColumn htlrr;
        private System.Windows.Forms.DataGridViewTextBoxColumn scan_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn jc_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn sbsn;
        private System.Windows.Forms.DataGridViewTextBoxColumn zczh;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghsmc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghskpxx;
        private System.Windows.Forms.DataGridViewTextBoxColumn ghslxfs;
        private System.Windows.Forms.DataGridViewTextBoxColumn cjmc;
        private System.Windows.Forms.DataGridViewTextBoxColumn cjlxfs;
        private System.Windows.Forms.DataGridViewTextBoxColumn ysrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn rzrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn zbq;
        private System.Windows.Forms.DataGridViewTextBoxColumn memo_note;
        private System.Windows.Forms.DataGridViewTextBoxColumn sblb;
        private System.Windows.Forms.DataGridViewTextBoxColumn tf_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn jk_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn bgd;
        private System.Windows.Forms.DataGridViewTextBoxColumn wmgsmc;
        private System.Windows.Forms.DataGridViewTextBoxColumn jyjyzm;
        private System.Windows.Forms.DataGridViewTextBoxColumn xcjyrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn zybkzpjb;
        private System.Windows.Forms.DataGridViewTextBoxColumn bf_flag;
        private System.Windows.Forms.DataGridViewTextBoxColumn htqdrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn htysrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn htlrrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn bfrq;
        private System.Windows.Forms.DataGridViewTextBoxColumn bfwjph;
    }
}